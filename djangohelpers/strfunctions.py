# -*- coding: utf-8 -*-
# misc string functions

import string


def has_rus(text):
    """Return true if text contain cyrilic"""
    if not text:
        return None

    rus = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"
    is_rus_ = False
    for l in rus:
        if l in text.lower():
            is_rus_ = True
    return is_rus_


def is_latin(text):
    """Return true if text contain only latin letters and/or numbers"""
    if not text:
        return None

    check_list = string.ascii_letters + "0123456789 "
    is_latin = True
    for l in text.lower():
        if l not in check_list:
            is_latin = False
    return is_latin


def fix_search_query(q):
    """remove special chars from user's query string"""
    q = q.replace("||", " ")
    chars_to_remove = {
        ord("."): " ",
        ord("|"): " ",
        ord("!"): " ",
        ord("?"): " ",
        ord("-"): " ",
        ord('"'): " ",
        ord("'"): " ",
        ord("("): " ",
        ord(")"): " ",
        ord("\\"): " ",
        ord("/"): " ",
        ord("@"): " ",
        ord("~"): " ",
        ord("*"): " ",
    }
    return q.translate(chars_to_remove)


def levenstein(str_1, str_2):
    n, m = len(str_1), len(str_2)
    if n > m:
        str_1, str_2 = str_2, str_1
        n, m = m, n

    current_row = range(n + 1)
    for i in range(1, m + 1):
        previous_row, current_row = current_row, [i] + [0] * n
        for j in range(1, n + 1):
            add, delete, change = (
                previous_row[j] + 1,
                current_row[j - 1] + 1,
                previous_row[j - 1],
            )
            if str_1[j - 1] != str_2[i - 1]:
                change += 1
            current_row[j] = min(add, delete, change)

    return current_row[n]
