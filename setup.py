from setuptools import setup

setup(name='djangohelpers',
      version='0.1.18',
      description='Some django helpers',
      url='https://bitbucket.org/pi11/django-misc-helpers',
      packages=['djangohelpers'],
      package_data={'djangohelpers': ['data/*.txt']},
      author="pi11",
      author_email="pi11@airmail.cc"
  # install_requires=['ipaddr'],
      )
