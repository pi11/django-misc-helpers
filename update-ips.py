import requests

operators = ["megafon", "beeline", "mts", "tele2", "AZ", "kievstar"]
base_url = "http://moblave.com/op_ip.php?op=%s"
for op in operators:
    r = requests.get(base_url % op)
    with open("djangohelpers/data/%s.txt" % op, "w+") as f:
        f.write(r.text)
